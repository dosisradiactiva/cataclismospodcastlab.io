---
layout: post  
title: "Cruising: Sexo en Espacios Públicos (Alex Espinoza)"  
date: 2020-09-18
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/19.png
podcast_link: https://archive.org/download/cruising_podcast/20.CRUISING
tags: [audio, adultos, cultura, Cataclismos]  
comments: true 
---
“El homosexual promiscuo es un revolucionario sexual. En cada momento de su existencia prohibida se enfrenta a leyes represivas, a morales represivas. Parques, callejones, túneles subterráneos, garajes, calles… son los campos de batalla” (John Rechy).

En este episodio Alex Espinoza, escritor del libro Cruising. Historia íntima de un pasatiempo radical (Dos Bigotes, 2020) nos adentrará en este mundo "underground" de la sexualidad.


<audio controls>
  <source src="https://archive.org/download/cruising_podcast/20.CRUISING.ogg" type="audio/ogg">
  <source src="https://archive.org/download/cruising_podcast/20.CRUISING.mp3" type="audio/mpeg">
</audio>

![#Cruising](http://cataclismospodcast.gitlab.io/images/19.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>