---
layout: post  
title: "Kamasutra: Todo lo que Deberías Saber"  
date: 2020-09-25
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/20.png
podcast_link: https://archive.org/download/kamasutra_podcast/21.Kamasutra
tags: [audio, podcast, cultura, Cataclismos]  
comments: true 
---
Díganme si sí o no, cuando nos mencionan al Kamasutra inevitablemente pensamos en posiciones y más posiciones para hacer el Delicioso, pero ¿sabes qué historia esconde este famosísimo libro?
En este episodio comentamos unos buenísimos datos que seguramente no conocías del más importante "consejero" del sexo de todos los tiempos.

<audio controls>
  <source src="https://archive.org/download/kamasutra_podcast/21.Kamasutra.ogg" type="audio/ogg">
  <source src="https://archive.org/download/kamasutra_podcast/21.Kamasutra.mp3" type="audio/mpeg">
</audio>

![#depresion](http://cataclismospodcast.gitlab.io/images/20.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>