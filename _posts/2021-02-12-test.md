---
layout: post  
title: "¿Te da Ansiedad la Diversidad? (Test)"  
date: 2021-02-12
categories: blog
image: http://cataclismospodcast.gitlab.io/images/33.png
podcast_link: https://archive.org/download/00.-test-para-saber-si-te-da-ansiedad-lo-diferente/00.%20Test%20Para%20Saber%20si%20te%20da%20ansiedad%20lo%20diferente
tags: [audio, adultos, cultura, Cataclismos]  
comments: true 
---
Bienvenidos al primer episodio de este año 2021. Comencemos con este test de introducción, con música de el Rival Más Débil, para saber si te da miedo la diversidad, lo diferente, para saber si te gusta vivir una vida plana. Y, si fuera el caso, no está mal, para eso estamos aquí, para crecer juntos y enriquecernos, para desaprender y aprender de la falsa información que nos han heredado.


<audio controls>
  <source src="https://archive.org/download/00.-test-para-saber-si-te-da-ansiedad-lo-diferente/00.%20Test%20Para%20Saber%20si%20te%20da%20ansiedad%20lo%20diferente.ogg" type="audio/ogg">
  <source src="https://archive.org/download/00.-test-para-saber-si-te-da-ansiedad-lo-diferente/00.%20Test%20Para%20Saber%20si%20te%20da%20ansiedad%20lo%20diferente%20%281%29.mp3" type="audio/mpeg">
</audio>

![#Test](http://cataclismospodcast.gitlab.io/images/33.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
