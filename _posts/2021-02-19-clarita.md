---
layout: post  
title: "La Comedia Como Catarsis Para Sanar (La Clarita, Comediante)"  
date: 2021-02-19
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/34.png
podcast_link: https://archive.org/download/la-clarita/La%20Clarita
tags: [audio, adultos, cultura, Cataclismos]  
comments: true 
---
En este episodio le damos la bienvenida a Clarita, comediante de humor negro en formato de Stand Up, así que no es apto para gente sensible. 
No te pierdas de la experiencia de Clarita en el mundo de la comedia, sus mejores y peores anécdotas...así que sin más, los dejo con el episodio para que lo disfruten.


<audio controls>
  <source src="https://archive.org/download/la-clarita/La%20Clarita.ogg" type="audio/ogg">
  <source src="https://archive.org/download/la-clarita/La%20Clarita%20%281%29.mp3" type="audio/mpeg">
</audio>

![#Clarita](http://cataclismospodcast.gitlab.io/images/34.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
