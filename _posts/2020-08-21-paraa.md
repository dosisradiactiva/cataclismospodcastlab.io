---
layout: post  
title: "Sólo Para Adultos"  
date: 2020-08-21
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/15.png
podcast_link: https://archive.org/download/soloparaadultos/Solo%20Para%20Adultos
tags: [audio, adultos, cultura, Cataclismos]  
comments: true 
---
“Si el sexo no fuese la cosa más importante de la vida, el Génesis no empezaría por ahí”. Cesare Pavese, escritor italiano.

Este es el primer episodio e introducción de la nueva temporada, Sólo Para Adultos, en la que exploraremos temas de sexualidad y erotismo.
Disfrútalo.


<audio controls>
  <source src="https://archive.org/download/soloparaadultos/Solo%20Para%20Adultos.ogg" type="audio/ogg">
  <source src="https://archive.org/download/soloparaadultos/Solo%20Para%20Adultos.mp3" type="audio/mpeg">
</audio>

![#SoloParaAdultos](http://cataclismospodcast.gitlab.io/images/15.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>