---
layout: post  
title: "Múestra tu mejor pose: Ball Culture (Kevin Rodriguez)"  
date: 2020-07-03
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/08.png
podcast_link: https://archive.org/download/ball-culture/Muestra%20tu%20mejor%20pose.%20Ball%20Culture
tags: [audio, lgbt, cultura, Cataclismos]  
comments: true 
---
Temporada 2: 
*En este episodio Kevin Rodriguez nos adentra en el mundo del Ballroom, estos espacios de integración que luchan por mantener viva una cultura de orgullo, protesta y autoexpresión. 

<audio controls>
  <source src="https://archive.org/download/ball-culture/Muestra%20tu%20mejor%20pose.%20Ball%20Culture.ogg" type="audio/ogg">
  <source src="https://archive.org/download/ball-culture/Muestra%20tu%20mejor%20pose.%20Ball%20Culture.mp3" type="audio/mpeg">
</audio>

![#Piloto](http://cataclismospodcast.gitlab.io/images/08.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>
